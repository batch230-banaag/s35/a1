const express = require("express");
const mongoose = require("mongoose");


const app = express(); //createServer()
const port = 3001;

// [SECTION] Setting up MongoDB Connection
// connect to the db by passing in your connection string
// remember to replace the password and database names with actual values

// Syntax:
    // mongoose.connect("<MongoDB connection string>" , {useNewUrlParser: true})

    // DB name is found at connection string "S35"
mongoose.connect("mongodb+srv://admin:admin@batch230.yvvrpsv.mongodb.net/S35?retryWrites=true&w=majority", 
{
    useNewUrlParser: true,
    useUnifiedTopology: true
})

// Connection to database
// allows to handle errors when the initial connection is established
// works with the "on" and "once" Mongoose methods
let db = mongoose.connection

// if a connection error occured, output an error msg in console
// console.error.bind(console) allow us to print errors in the browser console
    // and in the terminal
db.on("error", console.error.bind(console, "connection error"))
db.once("open", () => console.log("Connected to the cloud database"))


// Request Handler
app.use(express.json()) //body-parser = converts to JSON

// Creating a Schema
const taskSchema = new mongoose.Schema({
    name: String,
    status: {
        type: String,
        default: "pending"
    }
})

                           //"Task - collection name in MongoDB "
const Task = mongoose.model("Task", taskSchema) //Task as a model

app.post("/tasks", (request,response) => {

    // mongoose syntax "findOne"
    Task.findOne({name: request.body.name}, (err, result) => {

        //if there was a duplicate found and the document's name
        //matches the information via the client/Postman(testing)
        if(result!=null && result.name == request.body.name){

            return response.send("Duplicate task found");
        }
        
        else{
            
            let newTask = new Task({

                name: request.body.name
            })

            newTask.save((saveErr, savedTask) => {

                if(saveErr){
                    return console.error(saveErr)
                }
                else{
                    return response.status(201).send("New task created")
                }

            })
        }

    })
})


app.get("/tasks" , (req, res) => {
    
    Task.find({}, (err, result) => {

        if(err){
            return console.log(err)
        }
        else{
            return res.status(200).json({
                data: result
            })
        }
    })
})





// ACTIVITY ------------------------------------------------

const userSchema = new mongoose.Schema({
    username: String,
    password: String
})

const User = mongoose.model("User", userSchema) 

app.post("/signup", (request,response) => {

 
    User.findOne({name: request.body.username}, (err, result) => {

      
        if(result!=null && result.username == request.body.username){

            return response.send("Duplicate user found");
        }
        
        else{
            
            let newUser = new User({

                username: request.body.username,
                password: request.body.password
            })

            newUser.save((saveErr, savedTask) => {

                if(saveErr){
                    return console.error(saveErr)
                }
                else{
                    return response.status(201).send("New user registered")
                }

            })
        }

    })
})


app.get("/signup" , (req, res) => {
    
    User.find({}, (err, result) => {

        if(err){
            return console.log(err)
        }
        else{
            return res.status(200).json({
                data: result
            })
        }
    })
})


app.listen(port, ()=>console.log(`Server is running at port ${port}`))